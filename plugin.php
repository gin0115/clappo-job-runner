<?php declare( strict_types=1 );

namespace Clappo\Runner;

/*
Plugin Name: Clappo Job Runner
Plugin URI: https://www.clappo.co.uk
Description: Creates a job runner system which allows php jobs to be run outside of the users current session. Please vist clappo.co.uk for more details.
Author: Glynn Quelch
Version: 0.1.0
Author URI: https://www.clappo.co.uk
*/

/** Required Libs */
require_once "Lib/Database.php";
require_once "Lib/Admin.php";
require_once "Lib/RunnerProvider.php";

/** Helpers */
require_once 'loader.php';
require_once 'defaults.php';
