<?php declare( strict_types=1 );

namespace Clappo\Runner\View;

/**
 * Displays the reports of failed jobs.
 * 
 * @package Runner
 * @author Glynn Quelch <glynn.quelch@gmail.com>
 * @since 0.1.0
 */
?>
<div id="clappo-job-runner__failed">
</div>