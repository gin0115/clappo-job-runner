<?php declare( strict_types=1 );

namespace Clappo\Runner\Lib;

/**
 * The singleton which is created at run time.
 * 
 * @package Runner
 * @author Glynn Quelch <glynn.quelch@gmail.com>
 * @since 0.1.0
 */
final class RunnerProvider 
{
    
}
