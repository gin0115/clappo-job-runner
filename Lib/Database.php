<?php declare( strict_types=1 );

namespace Clappo\Runner\Lib;

/**
 * Handles all the interactions with the database.
 *
 * @package Runner
 * @author Glynn Quelch <glynn.quelch@gmail.com>
 * @since 0.1.0
 */
final class Database {


}
