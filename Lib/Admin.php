<?php declare( strict_types=1 );

namespace Clappo\Runner\Lib;

/**
 * Registers all interactions within wp-admin.
 * 
 * @package Runner
 * @author Glynn Quelch <glynn.quelch@gmail.com>
 * @since 0.1.0
 */
final class Admin 
{

}
